/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Product {

    private int productId;
    private Double maxPrice;
    private Double minPrice;
    private Double targetPrice;
    private Double modifiedTargetPrice;

    public Product(int productId, Double maxPrice, Double minPrice, Double targetPrice) {

        this.productId = productId;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
        this.targetPrice = targetPrice;
        this.modifiedTargetPrice = 0.0;
    }

    public Product() {
       
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(Double targetPrice) {
        this.targetPrice = targetPrice;
    }

    public Double getModifiedTargetPrice() {
        return modifiedTargetPrice;
    }

    public void setModifiedTargetPrice(Double modifiedTargetPrice) {
        this.modifiedTargetPrice = modifiedTargetPrice;
    }

    @Override
    public String toString() {
        return "Product{" + "productId = " + productId + ", maxPrice = " + maxPrice + ", minPrice = " + minPrice + ", targetPrice = " + targetPrice + '}';
    }

}
