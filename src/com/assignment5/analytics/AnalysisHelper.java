/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author manja
 */
public class AnalysisHelper {

    //analysis methods
    public void modifyTarget() {
        System.out.println("Q5.");
        Map<Integer, Double> averageSellingPrice = new HashMap<Integer, Double>();
        Map<Integer, List<String>> productSellingData = new HashMap<Integer, List<String>>();
        for (Item item : DataStore.getInstance().getItems().values()) {
            int currentProductId = item.getProductId();
            int currentQuantity = item.getQuantity();
            Double perProductPrice = item.getSalesPrice();
            Double sum = currentQuantity * perProductPrice;
            if (productSellingData.keySet().contains(currentProductId)) {
                currentQuantity += Integer.parseInt(productSellingData.get(currentProductId).get(0));
                sum += Double.parseDouble(productSellingData.get(currentProductId).get(1));
                productSellingData.get(currentProductId).set(0, String.valueOf(currentQuantity));
                productSellingData.get(currentProductId).set(1, String.valueOf(sum));
            } else {
                List<String> priceCountData = new ArrayList<String>();
                priceCountData.add(String.valueOf(currentQuantity));
                priceCountData.add(String.valueOf(sum));
                productSellingData.put(currentProductId, priceCountData);
            }
        }
        for (Map.Entry<Integer, List<String>> data : productSellingData.entrySet()) {
            double averagePrice = Double.parseDouble(data.getValue().get(1)) / Integer.parseInt(data.getValue().get(0));
            averagePrice = Math.round(averagePrice * 100);
            averagePrice = averagePrice / 100;
            averageSellingPrice.put(data.getKey(), averagePrice);
        }
        Map<Integer, Double> moreThanTarget = new HashMap<Integer, Double>();
        Map<Integer, Double> lessThanTarget = new HashMap<Integer, Double>();
        Map<Integer, Double> equalToTarget = new HashMap<Integer, Double>();
        for (Map.Entry<Integer, Product> p : DataStore.getInstance().getProducts().entrySet()) {
            if (!averageSellingPrice.keySet().contains(p.getKey())) {
                continue;
            }
            Double originalTargetPrice = p.getValue().getTargetPrice();
            Double averageSalePrice = averageSellingPrice.get(p.getKey());
            double difference = originalTargetPrice - averageSalePrice;
            difference = Math.round(difference * 100);
            difference = difference / 100;
            if (difference > 0) {
                lessThanTarget.put(p.getKey(), difference);
            } else if (difference < 0) {
                moreThanTarget.put(p.getKey(), difference);
            } else if (difference == 0) {
                equalToTarget.put(p.getKey(), difference);
            }
            double percentageDifference = difference / averageSalePrice * 100;
            if (percentageDifference > DataStore.percentageError || percentageDifference < (DataStore.percentageError * (-1))) {
                double min = Math.ceil(averageSalePrice * 0.95);
                double max = Math.floor(averageSalePrice * 1.05);
                p.getValue().setModifiedTargetPrice((double) Math.floor(ThreadLocalRandom.current().nextDouble(min, max + 1)));
            }
        }
        if (lessThanTarget.size() > 0) {
            System.out.println("===========================================================================================================");
            System.out.println("===========================================================================================================");
            System.out.println("Original Pricing Table (Target > Average sale price)");
            System.out.println("Product Id \t Average Sale Price \t Target Price \t Difference");
            lessThanTarget = sortHashMapByValues(Boolean.TRUE, lessThanTarget);
            for (Integer pid : lessThanTarget.keySet()) {
                String targetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getTargetPrice());
                System.out.println(pid + "\t\t\t" + averageSellingPrice.get(pid) + "\t\t    " + targetPrice + "\t  " + lessThanTarget.get(pid));
            }
        }
        if (moreThanTarget.size() > 0) {
            System.out.println("===========================================================================================================");
            System.out.println("===========================================================================================================");
            System.out.println("Original Pricing Table (Target < Average sale price)");
            System.out.println("Product Id \t Average Sale Price \t Target Price \t Difference");
            moreThanTarget = sortHashMapByValues(Boolean.FALSE, moreThanTarget);
            for (Integer pid : moreThanTarget.keySet()) {
                String targetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getTargetPrice());
                System.out.println(pid + "\t\t\t" + averageSellingPrice.get(pid) + "\t\t    " + targetPrice + "\t  " + moreThanTarget.get(pid));
            }
        }
        if (equalToTarget.size() > 0) {
            System.out.println("===========================================================================================================");
            System.out.println("===========================================================================================================");
            System.out.println("Original Pricing Table (Target == Average sale price)");
            System.out.println("Product Id \t Average Sale Price \t Target Price \t Difference");
            for (Integer pid : equalToTarget.keySet()) {
                String targetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getTargetPrice());
                System.out.println(pid + "\t\t\t" + averageSellingPrice.get(pid) + "\t\t    " + targetPrice + "\t  " + equalToTarget.get(pid));
            }
        }
        lessThanTarget.clear();
        moreThanTarget.clear();
        equalToTarget.clear();
        System.out.println("************************************************************************************************************");
        System.out.println("After Modification:");
        for (Map.Entry<Integer, Product> p : DataStore.getInstance().getProducts().entrySet()) {
            if (!averageSellingPrice.keySet().contains(p.getKey())) {
                continue;
            }
            Double modifiedTargetPrice = p.getValue().getModifiedTargetPrice().equals(0.0) ? p.getValue().getTargetPrice() : p.getValue().getModifiedTargetPrice();
            Double averageSalePrice = averageSellingPrice.get(p.getKey());
            double difference = modifiedTargetPrice - averageSalePrice;
            difference = Math.round(difference * 100);
            difference = difference / 100;
            if (difference > 0) {
                lessThanTarget.put(p.getKey(), difference);
            } else if (difference < 0) {
                moreThanTarget.put(p.getKey(), difference);
            } else if (difference == 0) {
                equalToTarget.put(p.getKey(), difference);
            }
        }
        if (lessThanTarget.size() > 0) {
            System.out.println("===========================================================================================================");
            System.out.println("===========================================================================================================");
            System.out.println("Modified Pricing Table (Modified Target > Average sale price)");
            System.out.println("Product Id \t Average Sale Price \t TargetPrice \t Modified Target Price \t Difference \t Error");
            lessThanTarget = sortHashMapByValues(Boolean.TRUE, lessThanTarget);
            for (Integer pid : lessThanTarget.keySet()) {
                String originalTargetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getTargetPrice());
                String targetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getModifiedTargetPrice());
                if (targetPrice.equals("0.0")) {
                    targetPrice = "Not Required";
                } else {
                    targetPrice = targetPrice + "      ";
                }
                double a = lessThanTarget.get(pid) / averageSellingPrice.get(pid) * 100;
                a = Math.round(a * 100);
                a = a / 100;
                String Error = String.valueOf(a);
                System.out.println(pid + "\t\t\t" + averageSellingPrice.get(pid) + "\t\t    " + originalTargetPrice + "\t\t" + targetPrice + "\t  " + lessThanTarget.get(pid) + "\t\t " + Error + "%");
            }
        }
        if (moreThanTarget.size() > 0) {
            System.out.println("===========================================================================================================");
            System.out.println("===========================================================================================================");
            System.out.println("Modified Pricing Table (Modified Target < Average sale price)");
            System.out.println("Product Id \t Average Sale Price \t TargetPrice \t Modified Target Price \t Difference \t Error");
            moreThanTarget = sortHashMapByValues(Boolean.FALSE, moreThanTarget);
            for (Integer pid : moreThanTarget.keySet()) {
                String originalTargetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getTargetPrice());
                String targetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getModifiedTargetPrice());
                if (targetPrice.equals("0.0")) {
                    targetPrice = "Not Required";
                } else {
                    targetPrice = targetPrice + "      ";
                }
                double a = moreThanTarget.get(pid) / averageSellingPrice.get(pid) * 100;
                a = Math.round(a * 100);
                a = a / 100;
                String Error = String.valueOf(a);
                System.out.println(pid + "\t\t\t" + averageSellingPrice.get(pid) + "\t\t    " + originalTargetPrice + "\t\t" + targetPrice + "\t  " + moreThanTarget.get(pid) + "\t\t " + Error + "%");
            }
        }
        if (equalToTarget.size() > 0) {
            System.out.println("===========================================================================================================");
            System.out.println("===========================================================================================================");
            System.out.println("Modified Pricing Table (Modified Target == Average sale price)");
            System.out.println("Product Id \t Average Sale Price \t TargetPrice \t Modified Target Price \t Difference \t Error");
            for (Integer pid : equalToTarget.keySet()) {
                String originalTargetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getTargetPrice());
                String targetPrice = String.valueOf(DataStore.getInstance().getProducts().get(pid).getModifiedTargetPrice());
                if (targetPrice.equals("0.0")) {
                    targetPrice = "Not Required";
                } else {
                    targetPrice = targetPrice + "      ";
                }
                double a = equalToTarget.get(pid) / averageSellingPrice.get(pid) * 100;
                a = Math.round(a * 100);
                a = a / 100;
                String Error = String.valueOf(a);
                System.out.println(pid + "\t\t\t" + averageSellingPrice.get(pid) + "\t\t    " + originalTargetPrice + "\t\t" + targetPrice + "\t  " + equalToTarget.get(pid) + "\t\t " + Error + "%");
            }
        }

    }

    public LinkedHashMap<Integer, Double> sortHashMapByValues(Boolean isDesc,
            Map<Integer, Double> passedMap) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Double> mapValues = new ArrayList<>(passedMap.values());
        if (isDesc) {
            Collections.sort(mapValues, Collections.reverseOrder());
            Collections.sort(mapKeys, Collections.reverseOrder());
        } else {
            Collections.sort(mapValues);
            Collections.sort(mapKeys);
        }

        LinkedHashMap<Integer, Double> sortedMap
                = new LinkedHashMap<>();

        Iterator<Double> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Double val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Double comp1 = passedMap.get(key);
                Double comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }

    //Q1 .1
    public void getTopThreeBestNegotiatedProducts() {
//        Map<Integer, Product> product = DataStore.getInstance().getProducts();
//        Map<Integer, Item> item = DataStore.getInstance().getItems();
//        Map<Integer, Double> item1 = new HashMap<>();
//        Map<Integer, Double> product1 = new HashMap<>();
//
//        double prod = 0;
//        double totalSum = 0;
//        double totalQuantity = 0;
//
//        for (Item i : item.values()) {
//            for (Item j : item.values()) {
//                System.out.println(i.getProductId() + "   " + j.getProductId());
//                if ((i.getProductId()) == (j.getProductId())) {
//                    prod = j.getQuantity() * j.getSalesPrice();
//                    totalSum += prod;
//                    totalQuantity += j.getQuantity();
//                }
//            }
//            if (item1.containsKey(i.getProductId())) {
//                continue;
//            } else {
//                item1.put(i.getProductId(), (totalSum / totalQuantity));
//            }
//            totalSum = 0;
//            totalQuantity = 0;
//        }
//
        Map<Integer, Double> product1 = new HashMap<>();
        Map<Integer, Product> product = DataStore.getInstance().getProducts();

        Map<Integer, Double> averageSellingPrice = new HashMap<Integer, Double>();
        Map<Integer, List<String>> productSellingData = new HashMap<Integer, List<String>>();
        for (Item item5 : DataStore.getInstance().getItems().values()) {
            int currentProductId = item5.getProductId();
            int currentQuantity = item5.getQuantity();
            Double perProductPrice = item5.getSalesPrice();
            Double sum = currentQuantity * perProductPrice;
            if (productSellingData.keySet().contains(currentProductId)) {
                currentQuantity += Integer.parseInt(productSellingData.get(currentProductId).get(0));
                sum += Double.parseDouble(productSellingData.get(currentProductId).get(1));
                productSellingData.get(currentProductId).set(0, String.valueOf(currentQuantity));
                productSellingData.get(currentProductId).set(1, String.valueOf(sum));
            } else {
                List<String> priceCountData = new ArrayList<String>();
                priceCountData.add(String.valueOf(currentQuantity));
                priceCountData.add(String.valueOf(sum));
                productSellingData.put(currentProductId, priceCountData);
            }
        }
        for (Map.Entry<Integer, List<String>> data : productSellingData.entrySet()) {
            double averagePrice = Double.parseDouble(data.getValue().get(1)) / Integer.parseInt(data.getValue().get(0));
            averagePrice = Math.round(averagePrice * 100);
            averagePrice = averagePrice / 100;
            averageSellingPrice.put(data.getKey(), averagePrice);
        }
        for (Product p : product.values()) {
            product1.put(p.getProductId(), (averageSellingPrice.get(p.getProductId()) - p.getTargetPrice()));
        }
        Map<Integer, Double> result = sortHashMapByValues(Boolean.TRUE, product1);
        System.out.println("===========================================================================================");
        System.out.println("===========================================================================================");
        System.out.println("Q1. Top 3 best negotiated products(weighted average of selling price for each order)");
        int y = 1;
        double max1 = 0;
        for (Integer productId : result.keySet()) {
            if (y >= 4) {
                break;
            }
            if (max1 != result.get(productId)) {
                max1 = result.get(productId);
                System.out.println(y + " Product id : " + productId + " Profit value above target: " + Math.round(result.get(productId)));
                y++;
            } else {
                System.out.println("  Product id : " + productId + " Profit value above target: " + Math.round(result.get(productId)));
            }
        }
    }

    //Q1.2
    public void getTopThreeBestNegotiatedProducts1() {
        Map<Integer, Double> productList = new HashMap<Integer, Double>();
        for (Item items : DataStore.getInstance().getItems().values()) {
            double totalQuantity = 0;
            if (items.getSalesPrice() > (DataStore.getInstance().getProducts().get(items.getProductId()).getTargetPrice())) {
                if (productList.containsKey(items.getProductId())) {
                    totalQuantity = productList.get(items.getProductId()) + items.getQuantity();
                    productList.put(items.getProductId(), totalQuantity);
                } else {
                    productList.put(items.getProductId(), (double) items.getQuantity());
                }
            }
        }
        productList = sortHashMapByValues(Boolean.TRUE, productList);
        int y = 1;
        double max1 = 0;
        System.out.println("===========================================================================================");
        System.out.println("===========================================================================================");
        System.out.println("Q1. Top 3 best negotiated products(comparing target & selling  price for each product)");
        for (Integer productId1 : productList.keySet()) {
            if (y >= 4) {
                break;
            }
            if (max1 != productList.get(productId1)) {
                max1 = productList.get(productId1);
                System.out.println(y + " Product id : " + productId1 + " Total quantity: " + Math.round(productList.get(productId1)));
                y++;
            } else {
                System.out.println("  Product id : " + productId1 + " Total quantity: " + Math.round(productList.get(productId1)));
            }
        }
    }

    //Q2
    public void topThreeCustomersAboutTarget() {
        int orderId = 0;
        Double salesPrice;
        Double targetPrice;
        int quantity = 0;
        int productId = 0;
        Order norder = new Order();
        Product nproduct = new Product();
        Double absvalue = 0.0;
        Double abssum = 0.0;
        int custId = 0;
        Map<Integer, Double> custdiff = new HashMap();
        Map<Integer, Double> sortedcustdiff = new HashMap();

        for (Map.Entry<Integer, List<Integer>> c : DataStore.getInstance().getCustomers().entrySet()) {
            custId = c.getKey();
            abssum = 0.0;

            List<Integer> orderList = c.getValue();
            for (int i = 0; i < orderList.size(); i++) {
                absvalue = 0.0;
                orderId = orderList.get(i);
                norder = DataStore.getInstance().getOrders().get(orderId);
                salesPrice = norder.getItem().getSalesPrice();
                quantity = norder.getItem().getQuantity();
                productId = norder.getItem().getProductId();
                nproduct = DataStore.getInstance().getProducts().get(productId);
                targetPrice = nproduct.getTargetPrice();
                absvalue = (Math.abs(salesPrice - targetPrice)) * quantity;
                abssum = abssum + absvalue;

            }
            custdiff.put(custId, abssum);
        }
        sortedcustdiff = sortHashMapByValues(Boolean.FALSE, custdiff);

        int counter = 1;
        double max1 = 0;
        System.out.println("===========================================================================================");
        System.out.println("===========================================================================================");
        System.out.println("Q2. Top 3 valueable customers who purchase about Target Price:");

        for (Integer c : sortedcustdiff.keySet()) {
            // System.out.print("CustomerId : "+e.getKey());
            //System.out.println("   Distance of the Customer about Target Price: "+e.getValue());
            //  counter++;
            // if(counter==3)break;
            if (counter >= 4) {
                break;
            }
            if (max1 != sortedcustdiff.get(c)) {
                max1 = sortedcustdiff.get(c);
                System.out.println(counter + " Customer id : " + c + " Distance of the customer about target price: " + sortedcustdiff.get(c));
                counter++;
            } else {
                System.out.println(counter + " Customer id : " + c + " Distance of the customer about target price: " + sortedcustdiff.get(c));
            }
        }
    }

    //Q3
    public void getTopThreeBestSalesPeople() {
        Map<Integer, List<Integer>> salesPerson = DataStore.getInstance().getSales();
        Map<Integer, Double> salesTargetCounter = new HashMap<>();

        for (Map.Entry<Integer, List<Integer>> data : salesPerson.entrySet()) {
            Integer salesId = data.getKey();
            List<Integer> orderList = data.getValue();
            for (int i : orderList) {
                Order order = DataStore.getInstance().getOrders().get(i);
                double productTargetPrice = DataStore.getInstance().getProducts().get(order.getItem().getProductId()).getTargetPrice();
//                if (order.getItem().getSalesPrice() > productTargetPrice) {
//                    if (salesTargetCounter.keySet().contains(salesId)) {
//                        double count = salesTargetCounter.get(salesId);
//                        count += 1;
//                        salesTargetCounter.put(salesId, count);
//                    } else {
//                        salesTargetCounter.put(salesId, 1.0);
//                    }
//                }
                double diff = (order.getItem().getSalesPrice() - productTargetPrice) * (order.getItem().getQuantity());

                if (salesTargetCounter.keySet().contains(salesId)) {
                    double count = salesTargetCounter.get(salesId);
                    count += diff;
                    salesTargetCounter.put(salesId, count);
                } else {
                    salesTargetCounter.put(salesId, diff);
                }
            }
            }

            Map<Integer, Double> result1 = sortHashMapByValues(Boolean.TRUE, salesTargetCounter);
            System.out.println("===========================================================================================");
            System.out.println("===========================================================================================");
            System.out.println("Q3. Top 3 best sales people");
            int y = 1;
            double max1 = 0;
            for (Integer productId : result1.keySet()) {
                if (y >= 4) {
                    break;
                }
                if (max1 != result1.get(productId)) {
                    max1 = result1.get(productId);
                    System.out.println(y + " Product id : " + productId + " Count above target: " + (int) Math.round(result1.get(productId)));
                    y++;
                } else {
                    System.out.println("  Product id : " + productId + " Count above target: " + (int) Math.round(result1.get(productId)));
                }
            

        }
    }
    //Q4

    public void totalRevenueAboveTarget() {
        int salesId;
        int orderId;
        Double salesPrice;
        Double targetPrice;
        int quantity = 0;
        int productId = 0;
        Double revenue = 0.0;
        Double diff = 0.0;
        Order norder = new Order();
        Product nproduct = new Product();
        for (Map.Entry<Integer, List<Integer>> s : DataStore.getInstance().getSales().entrySet()) {

            salesId = s.getKey();
            // System.out.print("\n"+"salesId:"+salesId);
            List<Integer> salesList = s.getValue();
            for (int i = 0; i < salesList.size(); i++) {

                diff = 0.0;
                orderId = salesList.get(i);
                norder = DataStore.getInstance().getOrders().get(orderId);
                salesPrice = norder.getItem().getSalesPrice();
                quantity = norder.getItem().getQuantity();
                productId = norder.getItem().getProductId();
                nproduct = DataStore.getInstance().getProducts().get(productId);
                targetPrice = nproduct.getTargetPrice();
                diff = (salesPrice - targetPrice) * quantity;
                /* System.out.println("\n");
                System.out.print("orderid:"+orderId);
                System.out.print("salesprice:"+salesPrice);
                System.out.print("targetprice:"+targetPrice);
                System.out.print("diff:"+diff);*/
                if (diff > 0) {
                    revenue = revenue + diff;
                    // System.out.print("revenue:"+revenue);
                }
            }
        }
        System.out.println("===========================================================================================");
        System.out.println("===========================================================================================");
        System.out.println("Q4. Total Revenue of the Company above Target Price");
        System.out.println("Revenue: " + revenue);
    }

}
